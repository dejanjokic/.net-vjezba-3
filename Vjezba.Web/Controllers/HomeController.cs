﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vjezba.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        /// <summary>
        /// Pogledati zadatak 3.1
        /// Ova akcija se poziva prilikom poziva: GET zahtjev na /Home/Contact
        /// Ova akcija kao rezultat vraća inicijalnu (praznu) formu za popuniti podatke
        /// Formu je potrebno kreirati kao dio zadatka
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "Jednostavan način proslijeđivanja poruke iz Controller -> View.";

            //Kao rezultat se pogled /Views/Home/Contact.cshtml renderira u "pravi" HTML
            //Primjetiti - View() je poziv funkcije koja uzima cshtml template i pretvara ga u HTML
            //Zasto bas Contact.cshtml? Jer se akcija zove Contact, te prema konvenciji se "po defaultu" uzima cshtml datoteka u folderu Views/CONTROLLER_NAME/AKCIJA.cshtml

            return View();
        }

        /// <summary>
        /// Ova akcija se poziva kada na formi za kontakt kliknemo "Submit"
        /// URL ove akcije je /Home/SubmitQuery, uz POST zahtjev isključivo - ne može se napraviti GET zahtjev zbog [HttpPost] parametra
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitQuery(FormCollection formData)
        {
            //Ovdje je potrebno obraditi podatke i pospremiti finalni string u ViewBag

            string Ime = formData.Get("ime");
            string Prezime = formData.Get("prezime");
            string Email = formData.Get("email");
            string Poruka = formData.Get("poruka");
            string TipPoruke = formData.Get("tipPoruke");

            string Newsletter = "nećemo vas obavijestiti";

            if (formData.Get("news") == "on")
                Newsletter = "obavijestit ćemo vas";

            ViewBag.Message = "Dragi " + Ime + " " + Prezime + " (" + Email + "), zaprimili smo Vašu poruku te će Vam se netko ubrzo javiti. Sadržaj Vaše poruke je: [" + TipPoruke + "] " + Poruka + ". Također, " + Newsletter + " o daljnjim promjenama putem newslettera.";


            //Kao rezultat se pogled /Views/Home/ContactSuccess.cshtml renderira u "pravi" HTML
            //Kao parametar se predaje naziv cshtml datoteke koju treba obraditi (ne koristi se default vrijednost)
            //Trazenu cshtml datoteku je potrebno samostalno dodati u projekt
            return View("ContactSuccess");
        }
    }
}